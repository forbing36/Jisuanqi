﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class History : Form
    {
        public History()
        {
            InitializeComponent();
        }

        private void button_Click_Copy(object sender, EventArgs e)
        {
            Clipboard.SetText(listBox1.Items[listBox1.SelectedIndex].ToString());
        }

        private void button_Click_Delete(object sender, EventArgs e)
        {
            //listBox1.ClearSelected();
            //listBox1.Items.Clear(); 清除全部
            int sel = listBox1.SelectedIndex;
            listBox1.Items.RemoveAt(sel);
        }

        private void History_Load(object sender, EventArgs e)
        {
            foreach (string a in Common.historylist)
            {
                listBox1.Items.Add(a);
            }
        }
    }
}
