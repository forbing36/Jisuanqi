﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }
        
        /*连等功能 
        ///存储最近输入的数字e_Num和最近的操作e_Op
        ///button CE 保存e_Num,而button C 清空e_Num和e_Op
        double e_Num = 0;
        double e_Res=0;
        string e_Op="";
        //*/
        string op="";//运算符
        double num1=0;
        double num2=0;
        double res=0;
        int iOp = 0;//运算符位置
        bool newOp = true;  //开始新的运算
        bool addOp = false;        
        //键入数字
        private void button_Click_Num(object sender, EventArgs e)    
        {
            if (newOp)
            {
                textNumBox.Text = "";
                newOp = false;
                addOp = false;
                /*
                e_Num = num2;
                e_Res=res;
                e_Op=op;
                //*/
            }
            ///*  获取button值，赋值给textNumBox.Ttext
            ///多个button的Click事件同时命名为button_Click_Num
            ///实现批量的button_Click_Num的操作
            Button tempNum = (Button) sender;
            textNumBox.Text += tempNum.Text.ToString();
            //*/
        }

        private void button_Click_Op(object sender, EventArgs e)
        {
            
            Button tempOp = (Button)sender;
            operater(tempOp.Text.ToString());
        }
        private void operater(string oper)
        {
            if (addOp)
            {
                textNumBox.Text = Convert.ToString(res) + oper;
                newOp = false;
            }
            else
            {
                textNumBox.Text += oper;
            } 
            op=oper;
            //e_Op=tempOp.Text.Tostring();
            //e_num1=textNumBox.Text.Substring(0, textNumBox.Text.Length - 1);
            //e_num2=textNumBox.Text.SkipWhile<char>(char i,result<char,i=='+'||i=='-'||i=='×'||i=='÷'>predicate);//并不会用该函数
        }
        private void operater(char oper)
        {
            if (addOp)
            {
                newOp = false;
            }
            op = Convert.ToString(oper);
            //e_Op=tempOp.Text.Tostring();
            //e_num1=textNumBox.Text.Substring(0, textNumBox.Text.Length - 1);
            //e_num2=textNumBox.Text.SkipWhile<char>(char i,result<char,i=='+'||i=='-'||i=='×'||i=='÷'>predicate);//并不会用该函数
        }


        private void button_Click_Del(object sender, EventArgs e)
        {
            if (textNumBox.Text.Length > 0)
            {
                textNumBox.Text = textNumBox.Text.Substring(0, textNumBox.Text.Length - 1);
                newOp = false;
            }
        }

        private void button_Click_CE(object sender, EventArgs e)
        {
            textNumBox.Text = "";
            newOp = true;
        }

        private void button_Click_C(object sender, EventArgs e)
        {
            textNumBox.Text = "";
            /*
            e_Num = 0;
            e_Op = "";
            //*/
        }
        private void button_Click_Result(object sender, EventArgs e)
        {
            try
            {
                //if (newOp)    不要在result里判断是否是重新开始的计算，而是更新textNumBox.Text
                //{
                    result();
                    res = Operate(op);
                    newOp = true;
                    addOp = true;
                    Common.historylist.Add(textNumBox.Text);

                //}
                //else
                //{
                //    result(res);
                //    res = Operate(op); 
                //}
            }
            catch (Exception exc)
            {
                textNumBox.Text= "Error";
                newOp = false;
                addOp = true;
            }
        }
        private void result()
        {
            iOp = textNumBox.Text.IndexOf(op);
            num1 = Convert.ToDouble(textNumBox.Text.Substring(0, iOp));
            num2 = Convert.ToDouble(textNumBox.Text.Substring(iOp + 1, textNumBox.Text.Length - iOp - 1));//-iOp,第二个参数是substring起始之后多少个
            
        }
        //private void result(double oldnum)
        //{
        //    iOp=textNumBox.Text.LastIndexOf(op);
        //    num1 = oldnum;
        //    num2 = Convert.ToDouble(textNumBox.Text.Substring(iOp + 1, textNumBox.Text.Length - iOp - 1));
        //}
        private double Operate(string opp)
        {
            double ress;
            if (op == "+")
            {
                ress = num1 + num2;
                textNumBox.Text += "=" + ress;
            }
            else if (op == "-")
            {
                ress = num1 - num2;
                textNumBox.Text += "=" + ress;
            }
            else if (op == "×")
            {
                ress = num1 * num2;
                textNumBox.Text += "=" + ress;
            }
            else if (op == "÷")
            {
                
                ress = num1 / num2;
                textNumBox.Text += "=" + ress;
            }
            else
            {
                ress = 0;
            }
            return ress;
        }

        private void button_Click_fushu(object sender, EventArgs e)
        {
            textNumBox.Text += "-";
            newOp = false;
            Common.historylist.Add(textNumBox.Text);
        }

        private void button_Click_pingfanggen(object sender, EventArgs e)
        {
            double numtemp;
            try
            {
                numtemp = Convert.ToDouble(textNumBox.Text);
            }
            catch (System.FormatException)  //不能转化为double的异常下用res结果计算
            {
                numtemp = res;
            }
            res = Math.Sqrt(numtemp);
            textNumBox.Text = "√（" + numtemp + ")=" + res;
            newOp = true;
            Common.historylist.Add(textNumBox.Text);
        }

        private void button_Click_daoshu(object sender, EventArgs e)
        {
            double numtemp;
            try
            {
                numtemp = Convert.ToDouble(textNumBox.Text);
            }
            catch (System.FormatException)  //不能转化为double的异常下用res结果计算
            {
                numtemp = res;
            }
            res = 1/numtemp;
            textNumBox.Text = "1/（" + numtemp + ")=" + Convert.ToString(res);
            newOp = true;
            Common.historylist.Add(textNumBox.Text);
        }

        private void Click_About(object sender, EventArgs e)
        {
            About aboutbox = new About();   //打开新的窗口，要先new 方法
            aboutbox.ShowDialog();
        }

        private void Click_History(object sender, EventArgs e)
        {
            History historybox = new History();   //打开新的窗口，要先new 方法
            historybox.ShowDialog();
        }

        private void Click_PasteNow(object sender, EventArgs e)
        {
            textNumBox.Text += Clipboard.GetText();
        }

        private void Click_CopyNow(object sender, EventArgs e)
        {
            Clipboard.SetText(textNumBox.Text);
        }

        bool flag = true;
        private void KeyPress_EqueKey(object sender, KeyPressEventArgs e)
        {
            
            switch (e.KeyChar)
            {
                case '=':
                    {
                        if (flag)
                        {
                            flag = false;
                            try
                            {
                                //if (newOp)    不要在result里判断是否是重新开始的计算，而是更新textNumBox.Text
                                {
                                    //textNumBox.Text += "=";
                                    //result();
                                    //if (textNumBox.Text.Length > 0)
                                    //{
                                    //    textNumBox.Text = textNumBox.Text.Substring(0, textNumBox.Text.Length - 2);
                                    //    newOp = false;
                                    //}
                                    iOp = textNumBox.Text.IndexOf(op);
                                    int op0 = textNumBox.Text[0];//(0);
                                    int op1 = textNumBox.Text[1];
                                    num1 = Convert.ToDouble(textNumBox.Text.Substring(0, iOp));
                                    //string temp = textNumBox.Text.Substring(iOp + 1, textNumBox.Text.Length - iOp-1);
                                    num2 = Convert.ToDouble(textNumBox.Text.Substring(iOp + 1, textNumBox.Text.Length - iOp - 1));//-iOp,第二个参数是substring起始之后多少个

                                    res = Operate(op);
                                    newOp = true;
                                    addOp = true;
                                    Common.historylist.Add(textNumBox.Text);

                                }
                                //else
                                //{
                                //    result(res);
                                //    res = Operate(op); 
                                //}
                            }
                            catch (Exception exc)
                            {
                                textNumBox.Text = "Error";
                                newOp = false;
                                addOp = true;
                            }
                        }
                        else
                        {
                            flag = true;
                            //textNumBox.Text.Substring(1, textNumBox.Text.Length - 1);
                        }
                        break;
                    }
                case '+':
                    {
                        operater('+');
                        break;
                    }
                case '-':
                    {
                        operater('-');
                        break;
                    }
                case '*':
                    {
                        operater('×');
                        break;
                    }
                case '/':
                    {
                        operater('÷');
                        break;
                    }
            }
        }
    }

    //历史记录传递
    public class Common
    {
        public static List<string> historylist = new List<string>();
        public static int a = 0;
    }
}
